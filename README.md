# 可嵌入web项目的用户登录注册模块，以jar的方式提供。
## 依赖项
该模块以servlet 3.0为基础，所以需要web项目运行在servlet 3.0的环境中。
## 使用步骤
1.  编译打包该项目，然后将该jar添加进web项目中  
2.  该模块提供登录和注册页面：/usersso/login.html
3.  该模块提供post请求的servlet：/usersso/login,/usersso/register,/usersso/logout
4.  添加如下配置文件到系统中  

## 配置文件
将该项目的resources/usersso.properties文件复制到业务系统的resources目录下或者ClassPath目录下，文件名不能修改，配置文件中key不能修改，然后修改相关内容：
```properties
#该文件用于业务系统集成该模块的时候使用，放到系统resources目录或者ClassPath目录下
driver = com.mysql.jdbc.Driver
url = jdbc:mysql://localhost:3306/mywork?zeroDateTimeBehavior=convertToNull&useUnicode=true&characterEncoding=utf8
user = root
pass = root
#用户表名
user_table = userinfo
#账号字段
field_useraccount = useraccount
#昵称字段（可选，没有的话就删除）
field_username = username
#邮箱字段（可选，没有的话就删除）
field_email = email
#密码字段
field_password = password
```