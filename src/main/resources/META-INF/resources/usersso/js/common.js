

function setUserInfo(){
	var useraccount = $.cookie("useraccount");
	var username = $.cookie("username");
	if(useraccount!=null&&useraccount!=""){
		$("#loginBtn").hide();
		$("#userProfile").show();
		$("#userProfile #un").html(username);
		$("#userProfile #ua").attr("href", "view/home.html");
	}else{
		$("#loginBtn").show();
		$("#userProfile").hide();
	}
	$('#loginModal').on('show.bs.modal', function () {
		$("#loginDiv").show();
		$("#regDiv").hide();
	});
}


function login(){
	var useraccount = $("#loginForm #useraccount").val();
	var password = $("#loginForm #password").val();
	$.post("/usersso/login", {"useraccount" : useraccount, "password" : password}, 
		function(data, status) {
			if(data.code==0){
				//登录成功
				/*$("#loginModal").modal('hide');
				$("#loginBtn").hide();
				$("#userProfile").show();
				var useraccount = $.cookie("useraccount");
				var username = $.cookie("username");
				console.log(useraccount+username);
				$("#un").html(username);
				$("#ua").attr("href", "view/home.html");
				getTopInfo();*/
				window.location.reload();
			}else{
				alert(data.info);
			}
		}
	);
}


function setFormEvent(){
	//设置注册表单事件
	$("#regForm").submit(function(){
		var useraccount = $("#regForm #useraccount").val();
		var username = $("#regForm #username").val();
		var email = $("#regForm #email").val();
		var password = $("#regForm #password").val();
		$.post("user", {"useraccount" : useraccount, "username" : username,
			"email" : email, "password" : password}, 
			function(data, status) {
				if(data=="ok"){
					alert("注册成功！已自动登录。");
					$("#loginModal").modal('hide');
					$("#loginBtn").hide();
					$("#userProfile").show();
					$("#un").html(username);
					$("#ua").attr("href", "view/home.html");
				}else{
					alert(data);
				}
			}
		);
		//阻止表单自动提交
		return false;
	});
};


function logout(){
	$.post("user/logout", {}, 
		function(data, status) {
			if(data=="ok"){
				$("#loginBtn").show();
				$("#userProfile").hide();
				$("#un").html("");
				$("#ua").attr("href", "#");
				$("#loginDiv").show();
				$("#regDiv").hide();
				$.cookie("useraccount", null ,{path:"/"});
				$.cookie("username", null ,{path:"/"});
				window.location.reload();
			}else{
				alert(data);
			}
		}
	);
}

function toReg(){
	$("#loginDiv").hide();
	$("#regDiv").show();
	$("#myModalLabel").html("用户注册");
	$("#regOrLogin").html('已有账号&nbsp;<a href="javascript:void(0)" onclick="toLogin()">现在登录</a>')
}

function toLogin(){
	$("#loginDiv").show();
	$("#regDiv").hide();
	$("#myModalLabel").html("用户登录");
	$("#regOrLogin").html('还没有账号&nbsp;<a href="javascript:void(0)" onclick="toReg()">立即注册</a>')
}
