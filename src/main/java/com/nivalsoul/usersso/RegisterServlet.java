package com.nivalsoul.usersso;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.db.Db;
import cn.hutool.db.Entity;
import cn.hutool.json.JSONUtil;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Map;

/**
 * @Author 空山苦水禅人
 * @Date 2018/11/1 21:52
 */
@WebServlet(name = "registerServlet", urlPatterns = {"/usersso/register"}, loadOnStartup = 1)
public class RegisterServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(RegisterServlet.class);

    @Override
    public void init() throws ServletException {
        Env.init();
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest)req;
        request.setCharacterEncoding("UTF-8");
        HttpServletResponse response = (HttpServletResponse)resp;
        HttpSession session = request.getSession();
        System.out.println("sessionId="+session.getId());
        String useraccount = request.getParameter("useraccount");
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        User user = new User(useraccount, username, email, SecureUtil.md5(password));
        log.info("save User:"+JSONUtil.toJsonStr(user));
        try {
            Map<String, String> map = Env.tableInfoMap;
            Entity entity = Entity.create(map.get("user_table"));
            entity.set(map.get("field_useraccount"), user.getUseraccount());
            entity.set(map.get("field_password"), user.getPassword());
            if(map.containsKey("field_username")){
                entity.set(map.get("field_username"), user.getUsername());
            }
            if(map.containsKey("field_email")){
                entity.set(map.get("field_email"), user.getEmail());
            }
            Env.dbUse.insert(entity);
            response.sendRedirect("/usersso/login.html");
        } catch (SQLException e) {
            e.printStackTrace();
            response.setContentType("text/html; charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.println("<h2>注册失败</h2>");
            out.println("<p>"+e.getMessage()+"</p>");
            out.close();
        }
 
    }

    
}
