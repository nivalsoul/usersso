package com.nivalsoul.usersso;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.db.Db;
import cn.hutool.db.Entity;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author 空山苦水禅人
 * @Date 2018/11/1 21:52
 */
@WebServlet(name = "loginServlet", urlPatterns = {"/usersso/login"}, loadOnStartup = 1)
public class LoginServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(LoginServlet.class);

    @Override
    public void init() throws ServletException {
        Env.init();
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest)req;
        request.setCharacterEncoding("UTF-8");
        HttpServletResponse response = (HttpServletResponse)resp;
        HttpSession session = request.getSession();
        System.out.println("sessionId="+session.getId());
        String useraccount = request.getParameter("useraccount");
        String password = request.getParameter("password");
        log.info("user: "+useraccount+" login...");
        User user = null;
        try {
            Map<String, String> map = Env.tableInfoMap;
            List<Entity> list = Env.dbUse
                    .findAll(Entity.create(map.get("user_table"))
                            .set(map.get("field_useraccount"), useraccount)
                    );
            if(list.size()>0){
                Entity entity = list.get(0);
                user = new User(useraccount,
                        entity.getStr(map.get("field_username")),
                        entity.getStr(map.get("field_email")),
                        entity.getStr(map.get("field_password")));
                if(user.getUsername()==null){
                    user.setUsername("");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("code", 0);
        result.put("info", "登录成功");
        if(user == null){
            result.put("code", 1);
            result.put("info", "用户名不存在");
        }else if(!user.getPassword().equals(SecureUtil.md5(password))){
            result.put("code", 2);
            result.put("info", "密码错误");
        }

        log.info("code=="+result.get("code"));
        if(result.get("code").toString().equals("0")){
            log.info("登录成功，设置session...");
            //设置session和cookie
            setSessionCookie(request, response, user);
            String back = request.getParameter("back");
            if(back == null || back.equals("")){
                back = "/";
            }
            response.sendRedirect(back);
        }else {
            response.setContentType("text/html; charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.println("<h2>" + result.get("info") + "</h2>");
            out.close();
        }
        
    }

    private void setSessionCookie(HttpServletRequest request,
                                  HttpServletResponse response, User user) {
        //设置session
        HttpSession session = request.getSession();
        session.setAttribute("useraccount", user.getUseraccount());
        session.setAttribute("username", user.getUsername());
        session.setMaxInactiveInterval(3*3600);
        //设置cookie
        Cookie cookie = new Cookie("useraccount",user.getUseraccount());
        // 不设置的话，则cookies不写入硬盘,而是写在内存,只在当前页面有用,以秒为单位
        cookie.setMaxAge(3*3600);
        //设置路径，这个路径即该工程下都可以访问该cookie 如果不设置路径，那么只有设置该cookie路径及其子路径可以访问
        cookie.setPath("/");
        response.addCookie(cookie);
        try {
            cookie = new Cookie("username", URLEncoder.encode(user.getUsername(), "UTF-8"));
            cookie.setMaxAge(3*3600);
            cookie.setPath("/");
            response.addCookie(cookie);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
