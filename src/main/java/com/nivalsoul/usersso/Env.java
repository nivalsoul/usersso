package com.nivalsoul.usersso;

import cn.hutool.db.Db;
import cn.hutool.db.ds.DSFactory;
import cn.hutool.setting.Setting;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author 空山苦水禅人
 * @Date 2018/11/4 16:01
 */
public class Env {
    public static boolean isInit = false;
    public static Map<String, String> tableInfoMap = new HashMap();
    public static DataSource ds = null;
    public static Db dbUse = null;
    
    public static void init(){
        if(!isInit){
            Setting props = new Setting("usersso.properties");
            for(Object key : props.keySet()){
                tableInfoMap.put(key.toString(), props.get(key).toString());
            }
            DSFactory dsFactory = DSFactory.create(props);
            ds = dsFactory.getDataSource();
            dbUse = Db.use(ds);
        }
    }
}
