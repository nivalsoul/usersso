package com.nivalsoul.usersso;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @Author 空山苦水禅人
 * @Date 2018/11/1 21:52
 */
@WebServlet(name = "logoutServlet", urlPatterns = {"/usersso/logout"}, loadOnStartup = 1)
public class LogoutServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(LogoutServlet.class);
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        doPost(req, resp);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest)req;
        request.setCharacterEncoding("UTF-8");
        HttpServletResponse response = (HttpServletResponse)resp;
        //设置session
        HttpSession session = request.getSession();
        String useraccount = (String) session.getAttribute("useraccount");
        String username = (String) session.getAttribute("username");
        log.info("user: "+ useraccount +" logout...");
        session.removeAttribute("useraccount");
        session.removeAttribute("username");
        //设置cookie
        Cookie cookie = new Cookie("useraccount",useraccount);
        // 不设置的话，则cookies不写入硬盘,而是写在内存,只在当前页面有用,以秒为单位
        cookie.setMaxAge(0);
        //设置路径，这个路径即该工程下都可以访问该cookie 如果不设置路径，那么只有设置该cookie路径及其子路径可以访问
        cookie.setPath("/");
        response.addCookie(cookie);
        try {
            cookie = new Cookie("username", URLEncoder.encode(username, "UTF-8"));
            cookie.setMaxAge(0);
            cookie.setPath("/");
            response.addCookie(cookie);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.sendRedirect("/");
        
    }


}
