package com.nivalsoul.usersso;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author 空山苦水禅人
 * @Date 2018/11/1 22:21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String useraccount;
    private String username = "";
    private String email;
    private String password;
    
}
